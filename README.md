This is a simple project to parse data from log based on regular expression
-----------------------------------------

This project have multiple main class
We have to run **TClLargeFileProcessing.java**

-----------------------------------------
- 1- Import project as maven project in sts
- 2- This project has lombok dependency so make sue your IDE has this dependency 
 	enable, otherwise delete all @data from all pojo class and add getter-setter,  toString etc.
- 3- Run **TClLargeFileProcessing.java** once. (It will terminate )
- 4- Go to run -> run configuration -> argument -> paste your input log file with full path
 		D:\....\....\....\seliLog_KM1349446G15_E_latest.log
- 5- run it. 
- 6- Go to same directory, there will be another file named **seliLog_KM1349446G15_E_latest.log_output.log** (output)
------------------------------------------

Since it is parsing log character by character so it may take time to provide output.