package com.sk.jpatest.utill;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

public class TclLargeFileProcessingLevel {

	public static final Logger log = LogManager.getLogger(TclLargeFileProcessingLevel.class);

	public static void main(String[] args) throws IOException {

		if (args.length > 0) {
		
		ObjectMapper om = new ObjectMapper();
		TclData tclData = new TclData();
		File inputFile = new File(args[0]);
		InputStream is = new FileInputStream(inputFile);
		char ch ;
		File output = new File(args[0]+"_output.log");
		FileWriter fw = new FileWriter(output);
		String text = "";
		int idx = 0;
		String textVal = "";
		ArrayList<String> al = new ArrayList<>();
		ArrayList<String> nestedRelation = new ArrayList<>();
		
		// start streaming data from fiel 
		while (is.available() > 0) {
			// process data character by character
			ch = (char)is.read();
			text = text + ch;
			String input = "";

			RelatedParts relatedPart = new RelatedParts();
			//remove unnecessary keywords

			if (text.contains("{does-not-have-attribute}")) {
				text.replaceAll("{does-not-have-attribute}", "");
			}if (text.contains("{repeated-object}")) {
				text.replaceAll("{repeated-object}", "");
			}
			
			if (text.contains("{has_part forward}")) {
				String tclPart = text.substring(0, text.indexOf("{has_part forward}"));
				tclData = getTclMainObject(tclPart);
				text = "";
				
				String childString = prepareChildString (is);
				System.out.println(childString);
				
				List<RelatedParts> relatedParts = getChildRelatedPart(childString, 1l, tclData.getObjIdDsRoot());
				tclData.setRelatedParts(relatedParts);
			}
		}
		
		String tclString = om.writeValueAsString(tclData);
		fw.write(tclString);
		fw.close();
		}

	}
	
	

	private static String prepareChildString(InputStream is) throws IOException {

		String txt = "";
		int bracketCnt = 0;
		while ( is.available() > 0) {
			
			char ch = (char)is.read();
			txt = txt + ch;
			if (ch == '{') {
				bracketCnt ++;
			} else if ( ch == '}') {
				bracketCnt --;
			}
			if (bracketCnt == 0 && !txt.trim().equals("")) {
				return txt;
			}
			
		}
		
		return null;
	}



	private static TclData getTclMainObject(String text) {
		
		TclData tclData = new TclData();
		Pattern pPart = Pattern.compile(RegexPatternConstant.languageCodePattern + ":");
		Pattern pitem = Pattern.compile("item:");

		Matcher partMatcher = pPart.matcher(text);
		Matcher itemMatcher = pitem.matcher(text);

		String relatedItem = "";
		List<String> parts = new ArrayList<>();
		
		while (itemMatcher.find()) {
			int s = itemMatcher.end();
			relatedItem = getValuedItem(text, s);
		}
		while (partMatcher.find()) {
			int s = partMatcher.start();
			String partsVal = getValuedItem(text, s);
			parts.add(partsVal);
		}
		
		tclData.setObjIdDsRoot(relatedItem);
		tclData.setParts(parts);
		
		return tclData;
	}



	private static List<RelatedParts> getChildRelatedPart(String s, long level, String parent) {

		RelatedParts relatedParts = new RelatedParts();
		List<RelatedParts> relaPartsList = new ArrayList<RelatedParts>();


		// get item value for current string
		Pattern relPattern = Pattern.compile("\\{rel_[0-9]* [0-9]* 0 ");
		Pattern pitem = Pattern.compile("item:");
		
		Matcher relMatcher = relPattern.matcher(s);
		Map<String, String> strMap = new HashMap<>();
		String itemName = "";
		List<String> relList = new ArrayList<>();
		try {
			while (relMatcher.find()) {

				if (!relList.contains(relMatcher.group())) {
					// set item name for current relation
					relList.add(relMatcher.group());
					log.info(relMatcher.group());
					int sindex = relMatcher.start();
					int eindex = getMatchingBracketIndex(s, sindex);

					String subString = s.substring(sindex, eindex);
					System.out.println(subString);
					
					Matcher itemMatcher = pitem.matcher(subString);
					if (itemMatcher.find()) {
						int sidx = itemMatcher.end();
						itemName = getValuedItem(subString, sidx);
						log.info("item:- " + itemName);
					}
					
					if (subString.contains("{has_part forward}")) {
						s = s.replace(subString, "");
						relMatcher = relPattern.matcher(s);
						itemMatcher = pitem.matcher(s);
						
						// creating child string and then use recursion
						RelatedParts childPart = new RelatedParts();
						int indx = subString.indexOf("{has_part forward}");
						String itemPart = subString.substring(0, indx);
						childPart = prepareTclData(itemPart, level, parent, itemName);
						
						int idx = subString.indexOf("{has_part forward}")+("has_part forward}".length());
						System.out.println(idx + " "+ subString.charAt(idx+2));
						int edx = getMatchingBracketIndex (subString, idx+2);
						System.out.println(edx + " " + subString.charAt(edx));
						String childString = subString.substring(idx+2, edx);
						childPart.setHasPartForward( getChildRelatedPart(childString, level+1, itemName) );
						
						relaPartsList.add(childPart);
						
					} else {
						RelatedParts re = prepareTclData(subString, level, parent, itemName);
						System.out.println(re);
						relaPartsList.add(re);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		//relatedParts.setHasPartForward(relaPartsList);
		return relaPartsList;
	}

	private static int getMatchingBracketIndex(String s, int i) {

		int bracketCount = 1;
		String x = "";
		for (int k = i + 1; i < s.length(); k++) {
			if (bracketCount == 0)
				return k;
			if (s.charAt(k) == '{')
				bracketCount++;
			else if (s.charAt(k) == '}')
				bracketCount--;

		}

		return 0;

	}

	private static RelatedParts prepareTclData(String text, long level, String parentName, String itemName) {

		RelatedParts relation = new RelatedParts();
		List<String> parts = new ArrayList<>();
		List<ManufacturedPart> manufacturedParts = new ArrayList<>();
		List<String> manufacturePartList = new ArrayList<>();
		String relatedItem = "";
		try {
			Pattern pPart = Pattern.compile(RegexPatternConstant.languageCodePattern + ":");
			// Pattern pPart = Pattern.compile("(en|fr|zh):");
			Pattern pitem = Pattern.compile("item:");
			Pattern pManuf = Pattern.compile("manufacturer_part:");
			Pattern pManNote = Pattern.compile("manufacturer_note}");

			Matcher partMatcher = pPart.matcher(text);
			Matcher itemMatcher = pitem.matcher(text);
			Matcher manufMatcher = pManuf.matcher(text);

			while (itemMatcher.find()) {
				int s = itemMatcher.end();
				relatedItem = getValuedItem(text, s);
			}
			while (partMatcher.find()) {
				int s = partMatcher.start();
				String partsVal = getValuedItem(text, s);
				parts.add(partsVal);
			}
			while (manufMatcher.find()) {
				int s = manufMatcher.end();
				String manufVal = getValuedItem(text, s);
				manufacturePartList.add(manufVal);
			}

			if (text.indexOf("manufacturer_note") >= 0) {

				if (!(text.charAt(text.indexOf("manufacturer_note") + "manufacturer_note".length() + 2) == '}')) {

					String subText = text.substring(text.indexOf("manufacturer_note"), text.length());

					int sidx = subText.indexOf('{');
					int bracketCnt = 0;
					int startIdx = 0;
					int endIdx = 0;
					List<String> manufacturedPartList = new ArrayList<>();
					for (int i = sidx; i < subText.length(); i++) {

						if (subText.charAt(i) == '{') {
							bracketCnt++;
							if (bracketCnt == 1) {
								startIdx = i;
							}
						} else if (subText.charAt(i) == '}') {
							bracketCnt--;
							if (bracketCnt == 0) {
								endIdx = i;
								manufacturedPartList.add((subText.substring(startIdx, endIdx + 1)).trim());
							} else if (bracketCnt < 0) {
								break;
							}
						}
					}

					for (String textString : manufacturedPartList) {
						ManufacturedPart manufacturedPart = getManufacturedPart(textString);
						manufacturedParts.add(manufacturedPart);
					}
				}
			}
			for (int i = 0; i < manufacturePartList.size(); i++) {
				manufacturedParts.get(i).setManufacturerPart(manufacturePartList.get(i));
			}
			
			relation.setManufacturedParts(manufacturedParts);
			relation.setParts(parts);
			relation.setItemName(itemName);
			relation.setParentItem(parentName);
			relation.setLevel(level);

		} catch (Exception e) {
			log.error(e);
		}

		return relation;

	}

	private static String getValuedItem(String text, int s) {
		for (int k = s; s < text.length(); k++) {
			if (text.charAt(k) == '{' || text.charAt(k) == '}') {
				return text.substring(s, k).trim();
			}
		}
		return null;
	}

	private static ManufacturedPart getManufacturedPart(String textString) {

		ManufacturedPart manufacturedPart = new ManufacturedPart();
		textString = textString.substring(1, textString.length() - 1);
		while (textString.indexOf('{') > -1)
			textString = cleanData(textString);
		String[] sList = textString.split(" ");

		manufacturedPart.setManufacturerObj(sList[0]);
		manufacturedPart.setManufacturerAmt(sList[1]);
		manufacturedPart.setManufacturerVer(sList[2]);

		manufacturedPart.setManufacturerLte(sList[3].replaceAll("_", ":"));

		manufacturedPart.setManufacturerRohs(sList[4]);
		manufacturedPart.setManufacturerCountry(sList[5].replaceAll("_", " "));
		manufacturedPart.setManufacturerMot(sList[6]);
		manufacturedPart.setManufacturerCert(sList[7]);
		manufacturedPart.setManufacturerNote(sList[8]);

		return manufacturedPart;
	}

	private static String cleanData(String s) {

		int sidx = s.indexOf('{');
		int eidx = s.indexOf('}');

		if (eidx - sidx == 1) {
			s = s.replace("{}", "null");
			return s;
		} else {
			String subS = s.substring(sidx + 1, eidx);
			subS = String.join("_", subS.split(" "));
			s = s.replace(s.substring(sidx, eidx + 1), subS);
			return s;
		}
	}

}
