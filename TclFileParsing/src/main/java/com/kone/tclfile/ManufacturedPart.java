package com.sk.jpatest.utill;

import lombok.Data;

@Data
public class ManufacturedPart {
	
	private String manufacturerPart;
	
	private String manufacturerObj;
	
	private String manufacturerAmt;
	
	private String manufacturerVer;
	
	private String manufacturerLte;
	
	private String manufacturerRohs;
	
	private String manufacturerCountry;
	
	private String manufacturerMot;
	
	private String manufacturerCert;
	
	private String manufacturerNote ;
	
}