package com.sk.jpatest.utill;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
class Member {
	private int level;
	private String name;
	private String parent;
	private List<Member> child;
	
}

public class MultiBranchTest {
	

	public static List<Member> addMember (List<String> names, String parentName, int level) {
		
		List<Member> memList = new ArrayList<>();
		
		for (String name : names ) {
			Member m = new Member(level, name, parentName, null);
			memList.add(m);
			if (name.equals("basu")) {
				String [] str = new String [] {"shona", "mona"};
				List<String> memberList = Arrays.asList(str);
				m.setChild(addMember(memberList, "basu", level+1));
			} else {
				return memList;
			}
		}
 		
		return memList;
	}

	public static void main(String[] args) throws Exception {

		ObjectMapper om = new ObjectMapper();
		Member p = new Member();
		p.setName("Kamleshwari");
		p.setParent(null);
		
		String [] str = new String [] {"basu", "shesho", "nandani"};
		List<String> memberList = Arrays.asList(str);
		List<Member> memList = addMember(memberList, "Kamleshwari", 0);
		p.setChild(memList);
		
		
		String strs = om.writer().writeValueAsString(p);
		System.out.println(strs);
	}

}
