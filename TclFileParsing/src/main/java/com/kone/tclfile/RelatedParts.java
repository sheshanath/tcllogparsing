package com.sk.jpatest.utill;

import java.util.List;

import lombok.Data;

@Data
public class RelatedParts {
	
	private Long level;
	
	private String itemName;
	
	private String parentItem;
	
	private List<String> parts;
	
	private List<ManufacturedPart> manufacturedParts;
	
	private List<RelatedParts> hasPartForward;
}
