package com.sk.jpatest.utill;

import java.util.List;

import lombok.Data;

@Data
public class TclData {

	private String objIdDsRoot;
	
	private List<String> parts;
	
	private List<RelatedParts> relatedParts;
	
}
