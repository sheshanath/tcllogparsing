package com.sk.jpatest.utill;

import java.util.List;

import lombok.Data;

@Data
public class HasPartForward {
	
	private String item;
	
	private String parentItem;
	
	private List<HasPartForward> hasChildPart;
	
}
