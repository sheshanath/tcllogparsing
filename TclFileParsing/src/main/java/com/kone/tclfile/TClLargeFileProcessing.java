//package com.sk.jpatest.utill;
//
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.FileWriter;
//import java.io.IOException;
//import java.io.InputStream;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.regex.Matcher;
//import java.util.regex.Pattern;
//
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//
//public class TClLargeFileProcessing {
//
//	public static final Logger log = LogManager.getLogger(TClLargeFileProcessing.class);
//	
//	public static void main(String[] args) throws IOException{
//		// TODO Auto-generated method stub
//		
//		if (args.length > 0) {
//			ObjectMapper om = new ObjectMapper();
//			TclData tclData = new TclData();
//			File inputFile = new File(args[0]);
//			InputStream is = new FileInputStream(inputFile);
//			char ch ;
//			File output = new File(args[0]+"_output.log");
//			FileWriter fw = new FileWriter(output);
//			String text = "";
//			int idx = 0;
//			String textVal = "";
//			ArrayList<String> al = new ArrayList<>();
//			ArrayList<String> nestedRelation = new ArrayList<>();
//			
//			while ( is.available() > 0 ) {
//				ch = (char)is.read();
//				text = text + ch;
//				Pattern relPattern = Pattern.compile("rel_[0-9]* [0-9]* 0 ");
//				Matcher relMatcher = relPattern.matcher(text);
//				while (relMatcher.find() ) {
//					if (! al.contains(relMatcher.group())) {
//						int endIdx = relMatcher.start()-1;
//						textVal = text.substring( idx, endIdx);
//						text = "{"+relMatcher.group();
//						idx = 0;
//						al.add(relMatcher.group());
//						nestedRelation.add(textVal);
//					}
//				}
//			}
//			
//			List<RelatedParts> relatedPartList = new ArrayList<>();
//			for (String i : nestedRelation) {
//				
//				RelatedParts relatedPart = prepareTclData(i);
//				relatedPartList.add(relatedPart);
//			}
//			
//			tclData.setRelatedParts(relatedPartList);
//			String tclString = om.writer().writeValueAsString(tclData);
//			System.out.println(tclString);
//			fw.write(tclString);
//			
//			fw.close();
//			is.close();
//			
//			
//		}
//		
//		
//	}
//
//	private static RelatedParts prepareTclData(String text) {
//
//		RelatedParts relation = new RelatedParts();
//		List<String> parts = new ArrayList<>();
//		List<ManufacturedPart> manufacturedParts = new ArrayList<>();
//		List<String> manufacturePartList = new ArrayList<>();
//		String relatedItem = "";
//		try {
//			Pattern pPart = Pattern.compile(RegexPatternConstant.languageCodePattern+":");
//			//Pattern pPart = Pattern.compile("(en|fr|zh):");
//			Pattern pitem = Pattern.compile("item:");
//			Pattern pManuf = Pattern.compile("manufacturer_part:");
//			Pattern pManNote = Pattern.compile("manufacturer_note}");
//			
//			Matcher partMatcher = pPart.matcher(text);
//			Matcher itemMatcher = pitem.matcher(text);
//			Matcher manufMatcher = pManuf.matcher(text);
//			
//			while (itemMatcher.find()) {
//				int s = itemMatcher.end();
//				relatedItem = getValuedItem (text, s);
//			}
//			while (partMatcher.find()) {
//				int s = partMatcher.start();
//				String partsVal = getValuedItem(text, s);
//				parts.add(partsVal);
//			}
//			while (manufMatcher.find()) {
//				int s = manufMatcher.end();
//				String manufVal = getValuedItem(text, s);
//				manufacturePartList.add(manufVal);
//			}
//			
//			if (text.indexOf("manufacturer_note") >= 0) {
//
//				if (!(text.charAt(text.indexOf("manufacturer_note") + "manufacturer_note".length() + 2) == '}')) {
//
//					String subText = text.substring(text.indexOf("manufacturer_note"), text.length());
//
//					int sidx = subText.indexOf('{');
//					int bracketCnt = 0;
//					int startIdx = 0;
//					int endIdx = 0;
//					List<String> manufacturedPartList = new ArrayList<>();
//					for (int i = sidx; i < subText.length(); i++) {
//
//						if (subText.charAt(i) == '{') {
//							bracketCnt++;
//							if (bracketCnt == 1) {
//								startIdx = i;
//							}
//						} else if (subText.charAt(i) == '}') {
//							bracketCnt--;
//							if (bracketCnt == 0) {
//								endIdx = i;
//								manufacturedPartList.add((subText.substring(startIdx, endIdx + 1)).trim());
//							} else if (bracketCnt < 0) {
//								break;
//							}
//						}
//					}
//
//					for (String textString : manufacturedPartList) {
//						ManufacturedPart manufacturedPart = getManufacturedPart(textString);
//						manufacturedParts.add(manufacturedPart);
//					}
//				}
//			}
//			for (int i = 0; i < manufacturePartList.size(); i ++) {
//				manufacturedParts.get(i).setManufacturerPart(manufacturePartList.get(i));
//			}
//			
//			relation.setManufacturedParts(manufacturedParts);
//			relation.setParts(parts);
//			relation.setRelatedPart(relatedItem);
//			
//		} catch (Exception e) {
//			log.error(e);
//		}
//		
//		return relation;
//	}
//
//	private static ManufacturedPart getManufacturedPart(String textString) {
//
//		ManufacturedPart manufacturedPart = new ManufacturedPart();
//		textString = textString.substring(1, textString.length()-1);
//		while (textString.indexOf('{') > -1 )
//			textString = cleanData (textString);
//		String [] sList = textString.split(" ");
//		
//		manufacturedPart.setManufacturerObj(sList[0]);
//		manufacturedPart.setManufacturerAmt(sList[1]);
//		manufacturedPart.setManufacturerVer(sList[2]);
//		
//		manufacturedPart.setManufacturerLte(sList[3].replaceAll("_", ":"));
//		
//		manufacturedPart.setManufacturerRohs(sList[4]);
//		manufacturedPart.setManufacturerCountry(sList[5].replaceAll("_", " "));
//		manufacturedPart.setManufacturerMot(sList[6]);
//		manufacturedPart.setManufacturerCert(sList[7]);
//		manufacturedPart.setManufacturerNote(sList[8]);
//		
//		
//		return manufacturedPart;
//	}
//
//	private static String cleanData(String s) {
//		
//		int sidx = s.indexOf('{');
//		int eidx = s.indexOf('}');
//		
//		if (eidx - sidx == 1) {
//			s = s.replace("{}", "null");
//			return s;
//		}
//		else {
//			String subS = s.substring(sidx + 1, eidx);
//			subS = String.join("_", subS.split(" "));
//			s = s.replace(s.substring(sidx, eidx + 1), subS);
//			return s;
//		}
//	}
//	
//	private static String getValuedItem(String text, int s) {
//		for (int k = s; s < text.length(); k ++) {
//			if (text.charAt(k) == '{' || text.charAt(k) == '}') {
//				return text.substring(s,k).trim();
//			}
//		}
//		return null;
//	}
//
//}
