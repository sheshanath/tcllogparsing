// //We are not using this file
//
//package com.sk.jpatest.utill;
//
//import java.io.BufferedReader;
//import java.io.File;
//import java.io.FileReader;
//import java.io.FileWriter;
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.regex.Matcher;
//import java.util.regex.Pattern;
//
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
//import org.springframework.util.StringUtils;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//
//public class TclDataParsing {
//	public static final Logger log = LogManager.getLogger(TclDataParsing.class);
//	
//	public static void main(String[] args) throws IOException{
//		
//		if (args.length > 0) {
//			
//			ObjectMapper om = new ObjectMapper();
//			TclData tclData = new TclData();
//			File logFile = new File(args[0]);
//			File outputFile = new File (args[0]+"_Output.log");
//			
//			BufferedReader br = new BufferedReader(new FileReader(logFile));
//			
//			List<String> logTextList = new ArrayList<>();
//			String text = "";
//			FileWriter fw = new FileWriter(outputFile);
//			
//			while( (text = br.readLine())  != null ) {
//				
//				if ( ! StringUtils.isEmpty(text) ) {
//					logTextList.add(text);
//					
//				}
//			}
//			
//			br.close();
//			
//			String rootItem = "";
//			for (String i : logTextList) {
//				if ( i.contains("obj_id_ds_root") ) {
//					int colonIdx = i.lastIndexOf(':');
//					rootItem = i.substring(colonIdx+1);
//					break;
//				}
//			}
//			
//			tclData = prepareTclData (logTextList.get(logTextList.size()-1));
//			tclData.setObjIdDsRoot(rootItem);
//			String tclDataJson = om.writer().writeValueAsString(tclData);
//			fw.write(tclDataJson);
//			
//			fw.close();
//		}
//		
//	}
//
//	private static TclData prepareTclData(String s) {
//		
//		TclData tclData = new TclData();
//		s = s.replaceAll("(\\{no-object\\})", "");
//		s = s.replaceAll("(\\{does-not-have-attribute\\})", "");
//		s = s.replaceAll("(\\{ok master \\})", "");
//
//	
//		List<String> relatedItem = new ArrayList<>();
//		Pattern relatedItemPattern = Pattern.compile("rel_[0-9]* [0-9]* 0 ");
//		Matcher rMatcher = relatedItemPattern.matcher(s);
//		
//		while (rMatcher.find()) {
//			relatedItem.add(rMatcher.group());
//		}
//		String individualLog = "";
//		
//		List<RelatedParts> relationList = new ArrayList<>();
//		
//		for (String i : relatedItem) {
//		//	System.out.println(i);
//			RelatedParts r = new RelatedParts();
//			individualLog = getIndividualLog (i, s);
//			r = getRelatedData (individualLog);
//			relationList.add(r);
//		}
//		
//		tclData.setRelatedParts(relationList);
//		return tclData;
//	}
//
//	private static RelatedParts getRelatedData(String text) {
//
//		RelatedParts relation = new RelatedParts();
//		List<String> parts = new ArrayList<>();
//		List<ManufacturedPart> manufacturedParts = new ArrayList<>();
//		List<String> manufacturePartList = new ArrayList<>();
//		String relatedItem = "";
//		
//		try {
//			Pattern pPart = Pattern.compile("(en|fr|zh)");
//			Pattern pitem = Pattern.compile("item:");
//			Pattern pManuf = Pattern.compile("manufacturer_part:");
//			Pattern pManNote = Pattern.compile("manufacturer_note}");
//			
//			Matcher partMatcher = pPart.matcher(text);
//			Matcher itemMatcher = pitem.matcher(text);
//			Matcher manufMatcher = pManuf.matcher(text);
//			
//			while (itemMatcher.find()) {
//				int s = itemMatcher.end();
//				relatedItem = getValuedItem (text, s);
//			}
//			while (partMatcher.find()) {
//				int s = partMatcher.start();
//				String partsVal = getValuedItem(text, s);
//				parts.add(partsVal);
//			}
//			while (manufMatcher.find()) {
//				int s = manufMatcher.end();
//				String manufVal = getValuedItem(text, s);
//				manufacturePartList.add(manufVal);
//			}
//			
//			if (text.indexOf("manufacturer_note") >= 0) {
//
//				if (!(text.charAt(text.indexOf("manufacturer_note") + "manufacturer_note".length() + 2) == '}')) {
//
//					String subText = text.substring(text.indexOf("manufacturer_note"), text.length());
//
//					int sidx = subText.indexOf('{');
//					int bracketCnt = 0;
//					int startIdx = 0;
//					int endIdx = 0;
//					List<String> manufacturedPartList = new ArrayList<>();
//					for (int i = sidx; i < subText.length(); i++) {
//
//						if (subText.charAt(i) == '{') {
//							bracketCnt++;
//							if (bracketCnt == 1) {
//								startIdx = i;
//							}
//						} else if (subText.charAt(i) == '}') {
//							bracketCnt--;
//							if (bracketCnt == 0) {
//								endIdx = i;
//								manufacturedPartList.add((subText.substring(startIdx, endIdx + 1)).trim());
//							} else if (bracketCnt < 0) {
//								break;
//							}
//						}
//					}
//
//					for (String textString : manufacturedPartList) {
//						ManufacturedPart manufacturedPart = getManufacturedPart(textString);
//						manufacturedParts.add(manufacturedPart);
//					}
//				}
//			}
//			
//			for (int i = 0; i < manufacturePartList.size(); i ++) {
//				manufacturedParts.get(i).setManufacturerPart(manufacturePartList.get(i));
//			}
//			
//			relation.setManufacturedParts(manufacturedParts);
//			relation.setParts(parts);
//			relation.setRelatedPart(relatedItem);
//			
//		}catch (Exception e) {
//			log.error(e.getMessage());
//		}
//		
//		return relation;
//	}
//
//	
//	
//	private static ManufacturedPart getManufacturedPart(String textString) {
//		
//		ManufacturedPart manufacturedPart = new ManufacturedPart();
//		textString = textString.substring(1, textString.length()-1);
//		while (textString.indexOf('{') > -1 )
//			textString = cleanData (textString);
//		String [] sList = textString.split(" ");
//		
//		manufacturedPart.setManufacturerObj(sList[0]);
//		manufacturedPart.setManufacturerAmt(sList[1]);
//		manufacturedPart.setManufacturerVer(sList[2]);
//		
//		manufacturedPart.setManufacturerLte(sList[3].replaceAll("_", ":"));
//		
//		manufacturedPart.setManufacturerRohs(sList[4]);
//		manufacturedPart.setManufacturerCountry(sList[5].replaceAll("_", " "));
//		manufacturedPart.setManufacturerMot(sList[6]);
//		manufacturedPart.setManufacturerCert(sList[7]);
//		manufacturedPart.setManufacturerNote(sList[8]);
//		
//		
//		return manufacturedPart;
//	}
//
//	private static String cleanData(String s) {
//		int sidx = s.indexOf('{');
//		int eidx = s.indexOf('}');
//		
//		if (eidx - sidx == 1) {
//			s = s.replace("{}", "null");
//			return s;
//		}
//		else {
//			String subS = s.substring(sidx + 1, eidx);
//			subS = String.join("_", subS.split(" "));
//			s = s.replace(s.substring(sidx, eidx + 1), subS);
//			return s;
//		}
//	}
//
//	private static String getValuedItem(String text, int s) {
//		for (int k = s; s < text.length(); k ++) {
//			if (text.charAt(k) == '{' || text.charAt(k) == '}') {
//				return text.substring(s,k).trim();
//			}
//		}
//		return null;
//	}
//
//	private static String getIndividualLog(String pat, String text) {
//
//		int sidx = text.indexOf(pat) -1;
//		int eidx = getMatchedBracketIndex (text, sidx);
//		if (eidx == Integer.MAX_VALUE) return null;
//		return text.substring(sidx, eidx+1);
//	}
//
//	private static int getMatchedBracketIndex(String text, int sidx) {
//
//		int cnt = 1;
//		for (int i = sidx+1; i < text.length(); i ++) {
//			if (cnt == 0) return i;
//			if (text.charAt(i) == '{') cnt ++;
//			else if (text.charAt(i) == '}') cnt --;
//		}
//		return Integer.MAX_VALUE;
//	}
//	
//	
//}
